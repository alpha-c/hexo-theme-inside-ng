const path = require('path');
const crypto = require('crypto');
const ts = require('typescript');
const fs = require('fs-extra');

exports.md5 = function (str, len = 6) {
  return crypto.createHash('md5').update(str).digest('hex').substring(0, len);
}

exports.base64 = function (str) {
  return Buffer.from(str).toString('base64').replace(/=/g, '').substring(0, 6);
}

exports.webpackStdout = function (stats) {
  return stats.toString({
    modules: false,
    children: false,
    chunks: false,
    hash: false,
    chunkModules: false,
    version: false,
    entrypoints: false,
    builtAt: false,
    assets: true,
    colors: true
  });
};

/**
 * @param {string} filename
 * @param {string} localName
 * @returns {string}
 */
exports.parseTs = function(filePath) {
  const source = fs.readFileSync(path.join(__dirname, '../src', filePath)).toString();
  const result = ts.transpileModule(source, { compilerOptions: { module: ts.ModuleKind.CommonJS }});
  const outPath = path.join(__dirname, `../dist/tmp/${exports.md5(source, 20)}.js`);
  fs.ensureFileSync(outPath);
  fs.writeFileSync(outPath, result.outputText);
  return require(outPath);
}

/**
 * @returns {(token:string) => string}
 */
exports.alphabetId = function(start) {
  const cache = {}
  let i = start || 0
  return function (token) {
    if (cache[token]) return cache[token]
    cache[token] = alphabet(i++)
    return cache[token]
  }
}

function alphabet(i) {
  if (i < 26)
    return String.fromCharCode(97 + i)

  return alphabet(~~(i / 26)) + alphabet(i % 26)
}
