const exec = require('child_process').exec;
const pkg = require('./package.json');

const pkgs = [
  ...Object.keys(pkg.dependencies).map(name => [name, '']),
  ...Object.keys(pkg.devDependencies).map(name => [name, '--dev'])
];

run();

function run() {
  const [pkgName, opt] = pkgs.shift();

  console.log('updating', pkgName, opt);

  exec(`yarn remove ${pkgName} ${opt}`, function (e, stdout, stderr) {
    exec(`yarn add ${pkgName} ${opt}`, function (e, stdout, stderr) {
      if (pkgs.length) run()
    })
  })
}
