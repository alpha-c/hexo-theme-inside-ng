const mockElement = {
  getBoundingClientRect(): any { return {} as any; },
  createDocumentFragment(): any { return {}; },
  addEventListener(type: string, callback: EventListenerOrEventListenerObject, option?: EventListenerOptions) { },
  removeEventListener(type: string, callback: EventListenerOrEventListenerObject, option?: EventListenerOptions) { },
  innerHTML: '',
  createElement(tagName: string): any { return {}; },
  querySelector(selector: string): any { return; },
  getElementById(id: string): any { return; },
  style: {},
  classList: {
    add() { },
    remove() { }
  }
};

mockElement.querySelector = mockElement.createElement = mockElement.getElementById = function () { return mockElement; };

export const g: {
  win: any;
  doc: any;
  loc: any;
  isServer: true;
} = {
  win: {
    ...mockElement,
  },
  doc: {
    createDocumentFragment() { return {}; },
    body: mockElement,
    ...mockElement,
  },
  loc: {
    origin: { length: -2 }
  },
  isServer: true,
};
