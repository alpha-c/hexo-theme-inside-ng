import { AfterViewInit, Directive, HostListener, Renderer2 } from '@angular/core';
import { Subscription } from 'rxjs';
import { AppService, DeviceService, LoaderService } from '../services';
import { animate, css, transitionEnd, xProperty, redraw } from '../utils';

@Directive({
  selector: '[isZoomable]'
})
export class ZoomableDirective implements AfterViewInit {
  private container: HTMLDivElement;
  private busy = false;
  private resizeSub: Subscription;

  private img: HTMLImageElement;
  private target: HTMLImageElement;
  private transforms: string[];

  constructor(
    private device: DeviceService,
    private loader: LoaderService,
    private app: AppService,
    private renderer: Renderer2
  ) {
    this.container = this.renderer.createElement('div');
    this.container.classList.add('φcommon__zoomable');
  }

  ngAfterViewInit() {
    this.container.addEventListener('click', () => {
      this.detatch();
    });
  }

  @HostListener('click', ['$event.target'])
  onTap(element: HTMLImageElement) {
    if (element.tagName !== 'IMG' || !~element.className.indexOf('φinject__blockimg') || this.busy) return;
    this.busy = true;

    this.target = element;
    this.img = element.cloneNode() as any;

    this.resizeSub = this.device.media.subscribe(() => {
      if (this.target) this.resize(true);
    });

    this.resize().then(() => {
      this.target.style.opacity = '0';

      this.container.appendChild(this.img);
      this.app.root.appendChild(this.container);

      // IMPORTANT: trigger a reflow
      redraw(this.img);

      transitionEnd(this.img, () => {
        this.busy = false;
      });
      this.container.classList.add('φcommon__zoomable-active');
      this.img.style[xProperty('transform', true)] = this.transforms[1];
    });
  }

  resize(refresh?: boolean): Promise<void> {
    return new Promise(resolve => {
      if (this.img.width > 0) resolve(this.img.width);
      else {
        this.loader.show();
        const img = new Image;
        img.addEventListener('load', () => {
          this.loader.hide();
          resolve(img.width);
        });
        img.addEventListener('error', () => {
          this.loader.hide();
          this.img = this.target = null;
          this.busy = false;
        });
        img.src = this.img.src;
      }
    })
      .then((originWidth: number) => {
        const rect = this.target.getBoundingClientRect();
        let originHeight, scale;
        if (originWidth > this.device.width) originWidth = this.device.width;
        scale = rect.width / originWidth;
        originHeight = rect.height / scale;
        if (originHeight > this.device.height) originHeight = this.device.height;

        this.transforms = [
          `scale3d(${scale.toFixed(3)}, ${scale.toFixed(3)}, 1)`,
          `scale3d(1, 1, 1) translate3d(${~~(-rect.left + (this.device.width - originWidth) / 2)}px, ${~~(-rect.top + (this.device.height - originHeight) / 2)}px, 0)`
        ];

        // Initial style
        css(this.img, {
          left: ~~rect.left + 'px',
          top: ~~rect.top + 'px',
          [xProperty('transform', true)]: this.transforms[refresh ? 1 : 0]
        } as any);
      });
  }

  detatch() {
    if (this.busy) return;
    this.busy = true;

    const scrollTop = this.container.scrollTop;
    const duration = (scrollTop > 100 ? 100 : ~~scrollTop) * 4;

    animate(
      this.container, 'scrollTop',
      { from: scrollTop, to: 0, duration: duration > 1000 ? 1000 : duration },
      () => {
        css(this.img, { [xProperty('transform', true)]: this.transforms[0] } as any);
        this.container.classList.remove('φcommon__zoomable-active');

        transitionEnd(this.img, () => {
          this.target.style.opacity = '';
          this.app.root.removeChild(this.container);
          this.container.removeChild(this.img);
          this.img = this.target = this.transforms = null;
          this.busy = false;
          this.resizeSub.unsubscribe();
        });
      }
    );
  }
}
