import { AfterViewInit, Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[isSnippet]'
})
export class SnippetDirective implements AfterViewInit {
  constructor(private er: ElementRef) { }

  ngAfterViewInit() {
    setTimeout(() => this.process());
  }

  process() {
    const list = this.er.nativeElement && this.er.nativeElement.getElementsByTagName('script');

    if (list && list.length) {
      Array.from(list).forEach((script: HTMLScriptElement) => {
        script.remove();
      });
    }
  }
}
