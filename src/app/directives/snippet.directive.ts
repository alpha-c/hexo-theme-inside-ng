import { AfterViewInit, Directive, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[isSnippet]'
})
export class SnippetDirective implements AfterViewInit {
  constructor(private er: ElementRef, private renderer: Renderer2) { }

  ngAfterViewInit() {
    setTimeout(() => this.process());
  }

  process() {
    const gist_regex = /^https?\:\/\/gist.github.com/;
    const list = this.er.nativeElement && this.er.nativeElement.getElementsByTagName('script');

    if (list && list.length) {
      Array.from(list).forEach((script: HTMLScriptElement) => {
        const parent = document.createElement('div');
        script.parentElement.insertBefore(parent, script);

        const { innerHTML: scriptCode, src: scriptSrc } = script;
        if (!scriptCode && !scriptSrc) return;

        // gist
        if (scriptSrc && scriptSrc.match(gist_regex)) {
          let iframe = this.renderer.createElement('iframe');
          iframe.style.display = 'none';
          iframe.onload = () => {
            let iframeDocument = iframe.contentDocument;
            if (!iframeDocument) return;

            const gist = this.renderer.createElement('div'),
              gistStyle = iframeDocument.querySelector('link[rel="stylesheet"]'),
              gistContent = iframeDocument.querySelector('.gist');

            if (gistStyle && gistContent) {
              const html = gistContent.cloneNode() as HTMLElement;
              html.innerHTML = gistContent.innerHTML;
              gist.appendChild(gistStyle.cloneNode());
              gist.appendChild(html);
            }
            parent.removeChild(iframe);
            parent.appendChild(gist);
            iframe = iframeDocument = null;
          };
          iframe.srcdoc = `<script src="${scriptSrc}"></script>`;
          parent.appendChild(iframe);
          return;
        }

        // other script
        else {
          const runScript = this.renderer.createElement('script');
          if (scriptSrc) {
            runScript.src = scriptSrc;
          } else {
            runScript.innerHTML = scriptCode;
          }
          parent.appendChild(runScript);
        }

        // XD
        parent.parentElement.removeChild(parent);
      });
    }
  }
}
