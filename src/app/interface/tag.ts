import { PageData, ListedPost } from './common';

export interface Tag {
  name: string;
  count: number;
}

export interface Tags extends PageData {
  name: string;
  data: ListedPost[];
}
