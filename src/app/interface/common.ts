import { Appearance } from "./appearance";

export interface PageData {
  current: number;
  per_page: number;
  total: number;
}

// a lite version post used on list data
export interface ListedPost {
  title: string;
  link: string;
  date: string;
}

export interface DateFormatted {
  ll: string;
  L: string;
  'MM-DD': string;
}

export interface InsideEvent {
  type: 'theme' | 'pv';
  data: Appearance | { [name: string]: any };
}

export enum Network { Pending, Error, Completed };
