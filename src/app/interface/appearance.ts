export interface Appearance {
  name?: 'default' | 'dark';
  accent_color: string;
  foreground_color?: string;
  border_color?: string;
  background?: string;
  sidebar_background?: string;
  card_background?: string;
  content_width?: string;
  'font.base'?: string;
    // sidebar author name
  'font.logo'?: string;
    // sidebar menu
  'font.menu'?: string;
  'font.heading'?: string;
    // archive
  'font.label'?: string;
    // code, pre
  'font.code'?: string;
    // print
  'font.print'?: string;

  /**
   * https://github.com/chriskempson/base16
   *
   * Exceptions:
   *
   * ```
   * if cardBg === [0] or [15] === [0], there will be border and transparent background
   *   if cardBg === [1] or [15] === [1], there will be transparent status bar and extra border below status bar
   * else it would be flat
   *   if cardBg === [1], let [1] be [0]
   * ```
   */
  highlight?: string[];
}
