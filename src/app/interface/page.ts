import { CopyrightConfig } from './config';
import { DateFormatted } from './common';

export interface Page {
  title: string;
  date: string;
  date_formatted: DateFormatted;
  updated: string;
  content: string;
  link: string;
  plink: string;
  toc?: boolean;
  comments?: boolean;
  reward?: boolean;
  copyright?: CopyrightConfig;
  dropcap?: boolean;
  meta?: boolean;
}
