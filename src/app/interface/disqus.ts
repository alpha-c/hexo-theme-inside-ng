export interface Disqus {
  /**
   * Tells the Disqus service your forum's shortname,
   * which is the unique identifier for your website as registered
   * on Disqus. If undefined , the Disqus embed will not load.
   *
   * Usage: Specify your forum shortname by replacing EXAMPLE in the following line of code:
   * s.src = '//EXAMPLE.disqus.com/embed.js';  // IMPORTANT: Replace EXAMPLE with your forum shortname!
   */
  shortname: string;

  /**
   * Tells the Disqus service how to identify the current page. When the Disqus
   * embed is loaded, the identifier is used to look up the correct thread.
   * If this.page.identifier is undefined, the page's URL will be used.
   * The URL can be unreliable, such as when renaming an article slug or
   * changing domains, so we recommend using your own unique way of identifying a thread.
   * Be careful not to assign multiple identifiers to the same thread URL,
   * as it will result in Identifier Conflict.
   *
   * Usage: Specify a string or an integer as your unique identifier.
   * This can be dynamically rendered server-side.
   * Benefits:
   *  - You'll be able to reference the same thread regardless of the URL where it is loaded.
   *
   * Example:
   * The following uses an article slug as the identifier.
   * `this.page.identifier = '/december-2010/the-best-day-of-my-life/'`;
   *
   * The following uses a unique id as an identifier.
   * `this.page.identifier = '2583573'`;
   *
   * Most likely you will be rendering the values dynamically server-side in
   * your platform or CMS. The following is an example using PHP.
   * `this.page.identifier = '<? php echo $my_identifier; ?>';`
   */
  identifier: string | number;

  /**
   * Tells the Disqus service the URL of the current page. If undefined,
   * Disqus will take the `window.location.href`. This URL is used to look up
   * or create a thread if this.page.identifier is undefined. In addition,
   * this URL is always saved when a thread is being created so that Disqus
   * knows what page a thread belongs to.
   *
   * While the window.location.href is used in absence of this.page.url,
   * we highly recommend defining this variable. If a user visits your page at
   * the URL `http://example.com/helloworld.html?123`, Disqus may in fact load
   * a different thread than if the user came from
   * `http://example.com/helloworld.html`.
   *
   * To make sure the right thread is always displayed, you should define on your page,
   * using an absolute URL, `this.page.url = 'http://example.com/helloworld.html';`
   *
   * Please note that an absolute URL is required for the this.page.url variable.
   * Using a relative URL for this variable may prevent Disqus from loading
   * successfully on the page.
   */
  url: string;

  /**
   * Tells the Disqus service the title of the current page. This is used when
   * creating the thread on Disqus for the first time. If undefined, Disqus will
   * use the `<title>` attribute of the page. If that attribute could not be used,
   * Disqus will use the URL of the page.
   *
   * Usage: Specify a string as your page's unique title. This can be dynamically
   * rendered server-side.
   *
   * Benefits:
   *  - You can set a friendlier title to be shown in Discovery.
   *  - Your thread's title is set instantly rather than waiting on the Disqus system queue.
   *
   * Having comment threads titled http://example.com/helloworld.html isn't very pretty!
   */
  title: string;

  /**
   * Tells the Disqus service the category to be used for the current page.
   * This is used when creating the thread on Disqus for the first time.
   *
   * Categories are primarily used with our API for results filtering; categories
   * are not used for moderation (e.g., to filter comments by category in the moderation panel).
   * New categories can be created with our categories API endpoints. If you try
   * using a category ID that hasn't been created within your forum settings,
   * you'll receive a 400 Bad Request error.
   *
   * JavaScript Usage: Specify a category ID (not title). If undefined,
   * Disqus will use the forum's default General category.
   *
   * Example: `this.page.category_id = '123456'; // using category Sports which has ID 123456`
   *
   * API Usage: Filter results by category.
   *
   * Example: Utilize the `categories/listPosts` endpoint (or the `category` parameter
   * in conjunction with the `posts/list` endpoint) to list comments only from a certain category.
   */
  category_id: string | number;
}
