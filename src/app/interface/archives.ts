import { PageData, ListedPost } from './common';

export interface Archive {
  year: string;
  months: [
    {
      month: string;
      entries: ListedPost[]
    }
  ];
}

export interface Archives extends PageData {
  data: Archive[];
}
