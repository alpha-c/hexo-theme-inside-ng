import { PageData, ListedPost } from './common';

export interface Category {
  name: string;
  count: number;
}

export interface Categories extends PageData {
  data: ListedPost[];
  name: string;
}
