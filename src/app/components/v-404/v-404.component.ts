import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, first } from 'rxjs/operators';
import { random } from '../../utils';

@Component({
  selector: 'is-v-404',
  templateUrl: './v-404.component.html',
  host: { class: 'φv-404' },
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class V404Component implements OnDestroy {
  url: string;
  intendUrl: string;
  emoji: string;
  private routerSub: Subscription;
  private emojis = `(='X'=) (;-;) (^_^)b \(^Д^)/ (o^^)o (≥o≤) (·_·) (>_<) (^-^*) (˚Δ˚)b \(o_o)/ (·.·)`.split(' ');

  constructor(private router: Router) {
    this.routerSub = this.router.events.pipe(filter(event => event instanceof NavigationEnd), first())
      .subscribe((event: NavigationEnd) => {
        this.intendUrl = decodeURIComponent(event.url);
        this.url = decodeURIComponent(event.urlAfterRedirects);
        this.emoji = this.emojis[random(11)];
      });
  }

  ngOnDestroy() {
    this.routerSub.unsubscribe();
  }
}
