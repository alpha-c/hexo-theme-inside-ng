import { Directive, OnDestroy } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable, Subscription } from "rxjs";
import { InsideEvent } from "../interface/common";
import { AppService, DeviceService } from "../services";

@Directive()
export class HPage<T> implements OnDestroy {
  protected eventSub: Subscription;
  data$: Observable<T>;
  plugins: string[];
  pv: { [name: string]: any } = {};

  constructor(public app: AppService, protected route: ActivatedRoute, protected router: Router, protected device: DeviceService) {
    this.data$ = route.data as Observable<T>;
    this.plugins = this.app.getPlugins(route.snapshot.data.id);
    this.eventSub = this.app.event.subscribe((ev: InsideEvent) => {
      if (ev.type === 'pv') this.pv = ev.data;
    });
  }
  ngOnDestroy() {
    this.eventSub.unsubscribe();
  }
}
