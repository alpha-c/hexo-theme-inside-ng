import { AfterViewInit, Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { Category } from '../../interface';
import { animate } from '../../utils';
import { HPage } from '../shared';

const SCROLL_LIMIT_THRESHOLD = 10;

@Component({
  selector: 'is-v-category',
  templateUrl: './v-category.component.html',
  host: { class: 'v-category' },
})
export class VCategoryComponent extends HPage<{ categoryList: Category[] }> implements AfterViewInit, OnDestroy {
  @ViewChild('nav') private navRef: ElementRef;
  private scrollLimits = [0, 0];
  private scrollStep: number;
  private resizeSub: Subscription;
  showPrev: boolean;
  showNext: boolean;

  detect = () => {
    const ele: HTMLElement = this.navRef.nativeElement;
    const [min, max] = this.scrollLimits;
    this.showNext = ele.scrollLeft - max < -SCROLL_LIMIT_THRESHOLD;
    this.showPrev = ele.scrollLeft - min > SCROLL_LIMIT_THRESHOLD;
  }

  /**
   * @param direction  1 | -1
   */
  step(direction: number) {
    const ele: HTMLElement = this.navRef.nativeElement;
    const from = ele.scrollLeft;
    const to = from + this.scrollStep * direction;

    animate(ele, 'scrollLeft', {
      from,
      to: Math.min(Math.max(to, this.scrollLimits[0]), this.scrollLimits[1]),
      duration: Math.min(Math.max(Math.abs(from - to), 100), 500)
    });
  }

  ngAfterViewInit() {
    const ele: HTMLElement = this.navRef.nativeElement;
    const onResize = () => {
      this.scrollStep = ele.offsetWidth;
      this.scrollLimits[1] = ele.scrollWidth - this.scrollStep;
    };

    setTimeout(() => {
      const activeLink: HTMLElement = ele.querySelector('.φv-category__nav-link--active');
      if (activeLink) {
        activeLink.scrollIntoView
          ? activeLink.scrollIntoView()
          : activeLink.focus();
      }

      onResize();
      this.detect();

      if (this.showNext || this.showPrev) {
        this.resizeSub = this.device.media.subscribe(() => onResize());
        ele.addEventListener('scroll', this.detect);
      }
    }, 0);
  }

  ngOnDestroy() {
    this.eventSub.unsubscribe();
    if (this.showNext || this.showPrev) {
      this.resizeSub.unsubscribe();
      this.navRef.nativeElement.removeEventListener('scroll', this.detect);
    }
  }
}
