import { AfterViewInit, Component, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { g } from '../../global';
import { InsideEvent } from '../../interface/common';
import { Appearance, Config } from '../../interface';
import { AppService, DeviceService } from '../../services';

enum Theme { Default = 'default', Dark = 'dark' };

function getDefaultTheme(): Theme {
  if (g.isServer) return Theme.Default;
  const t = localStorage.getItem('__inside__');
  if (t) {
    try {
      return JSON.parse(t).theme.name;
    } catch {
      return Theme.Default;
    }
  }
}

@Component({
  selector: 'is-sidebar',
  templateUrl: './sidebar.component.html',
  host: { 'class': 'φsidebar' },
})
export class SidebarComponent implements AfterViewInit, OnDestroy {
  config: Config;
  activePath: string;
  plugins: string[];
  avatarPlugin: string;
  theme: Appearance['name'] = getDefaultTheme();
  private eventSub: Subscription;

  @ViewChild('inner', { static: true }) innerRef: ElementRef;

  constructor(
    public er: ElementRef,
    private route: ActivatedRoute,
    private router: Router,
    private device: DeviceService,
    private app: AppService,
  ) {
    this.config = this.app.config;
    this.plugins = this.app.getPlugins('sidebar');
    this.avatarPlugin = (this.app.getPlugins('avatar') || [])[0];
    this.eventSub = this.app.event.subscribe((ev: InsideEvent) => {
      if (ev.type === 'theme') {
        if (ev.data.name === Theme.Dark || ev.data.name === Theme.Default)
          this.theme = ev.data.name;
      }
    });

    if (this.config.menu) {
      this.router.events.pipe(filter(event => event instanceof NavigationEnd))
        .subscribe(() => {
          const { data, routeConfig } = this.route.snapshot.children[0];

          if (data.id === 'page') this.activePath = routeConfig.path;
          else if (data.id === 'home') this.activePath = '';
          else if (data.id === 'category') this.activePath = 'categories';
          else if (data.id === 'tag') this.activePath = 'tags';
          else this.activePath = data.id;
          if (this.activePath[0] !== '/') this.activePath = '/' + this.activePath;
        });
    }
  }

  ngAfterViewInit() {
    this.device.media.subscribe(() => {
      this.adjustFooter();
    });
    this.device.refreshMedia();
  }

  ngOnDestroy() {
    this.eventSub.unsubscribe();
  }

  private adjustFooter() {
    const footer = this.innerRef.nativeElement.children[1],
      height = this.innerRef.nativeElement.children[0].offsetHeight + footer.offsetHeight;

    footer.classList[height > this.device.height ? 'remove' : 'add']('φsidebar__footer--fixed');
  }

  toggleTheme() {
    const theme = this.theme === Theme.Default ? Theme.Dark : Theme.Default;
    const config = this.config.theme[theme];
    config.name = theme;
    document.dispatchEvent(new CustomEvent('inside', {
      detail: {
        type: 'theme',
        data: config
      }
    }));
  }
}
