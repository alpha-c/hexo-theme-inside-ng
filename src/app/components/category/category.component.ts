import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Categories } from '../../interface';

@Component({
  selector: 'is-category',
  templateUrl: './category.component.html',
  host: { class: 'φcategory' },
})
export class CategoryComponent implements OnInit {
  categories: Categories;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data
      .subscribe(data => this.categories = data.category);
  }

}
