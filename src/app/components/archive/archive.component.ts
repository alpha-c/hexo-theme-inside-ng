import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'is-archive',
  templateUrl: './archive.component.html',
  host: { class: 'φarchive' },
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ArchiveComponent {
  @Input() posts: any;
  @Input() dateFormat: string;
}
