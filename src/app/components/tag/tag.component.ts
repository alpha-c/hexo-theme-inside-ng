import { Component, ElementRef, Input, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'is-tag',
  templateUrl: './tag.component.html',
  host: { class: 'φtag' },
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TagComponent implements OnInit {
  @Input() name;
  @Input() count;

  constructor(private element: ElementRef) { }
  ngOnInit() {
    let fontSize = (.9 + this.count * .1);
    fontSize = fontSize > 4 ? 4 : fontSize;
    this.element.nativeElement.style.fontSize = fontSize + 'rem';
  }
}
