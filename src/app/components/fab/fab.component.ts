import { Component, EventEmitter, Input, OnInit, Output, ViewChild, ElementRef } from '@angular/core';
import { ScreenType } from '../../interface';
import { DeviceService, LoaderService } from '../../services';
import { animationEnd, redraw } from '../../utils';

export enum FabAction {
  'toTop',
  'toBottom',
  'toggleSidebar',
  'toggleToc',
  'search'
}

@Component({
  selector: 'is-fab',
  templateUrl: './fab.component.html',
  host: { class: 'φfab' },
})
export class FabComponent implements OnInit {
  sidebar: boolean;
  showMenu = false;
  private busy = false;
  @Input() progress: number;
  @Input() toc: boolean;
  @Input() search: boolean;
  @Output() action = new EventEmitter<FabAction>();
  @ViewChild('control', { static: true }) private controlRef: ElementRef<HTMLElement>;

  constructor(
    private device: DeviceService,
    private loader: LoaderService
  ) { }

  ngOnInit() {
    // subscribe window resize
    this.device.media.subscribe(screen => this.sidebar = screen.type !== ScreenType.lg);
    this.device.refreshMedia();

    // subscribe loader
    this.loader.state.subscribe(busy => {
      if (busy && !this.busy) this.rotate();
    });
  }

  act(action: string) {
    this.action.emit(FabAction[action]);
  }

  rotate() {
    const control = this.controlRef.nativeElement;
    this.busy = true;
    control.classList.add('φfab__control--rotating');
    animationEnd(control, () => {
      control.classList.remove('φfab__control--rotating');
      this.busy = false;
      redraw(control);
      // network busy, toggle animation state to trigger animate again
      if (this.loader.busy) this.rotate();
    });
  }

  toggle() {
    this.showMenu = !this.showMenu;
  }

  open() {
    this.showMenu = true;
  }

  close() {
    this.showMenu = false;
  }
}
