import { Component } from '@angular/core';
import { Post } from '../../interface';
import { HPage } from '../shared';

@Component({
  selector: 'is-v-post',
  templateUrl: './v-post.component.html',
  host: { class: 'φv-post' },
})

export class VPostComponent extends HPage<{ post: Post }> { }
