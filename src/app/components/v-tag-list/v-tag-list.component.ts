import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Tag } from '../../interface';
import { HPage } from '../shared';

@Component({
  selector: 'is-v-tag-list',
  templateUrl: './v-tag-list.component.html',
  host: { class: 'φv-tag-list' },
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VTagListComponent extends HPage<{ tagList: Tag[] }> { }
