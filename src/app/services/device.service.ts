import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Screen, ScreenType } from '../interface';
import { debounce } from '../utils';
import { g } from '../global';

export interface ScrollState {
  scrollTop: number;
}

@Injectable({
  providedIn: 'root',
})
export class DeviceService {
  private mediaSubject = new Subject<Screen>();
  private scrollSubject = new Subject();
  private host: HTMLElement;

  media = this.mediaSubject.asObservable();
  scroll = this.scrollSubject.asObservable();

  width: number = g.win.innerWidth;
  height: number = g.win.innerHeight;

  constructor() {
    g.win.addEventListener('resize', debounce(() => {
      this.refreshMedia();
    }, 500), { passive: true });
  }


  initScroll(host: HTMLElement) {
    this.host = host;
    host.addEventListener('scroll', () => {
      this.refreshScroll({ scrollTop: host.scrollTop });
    }, { passive: true });
  }
  refreshScroll(state?: ScrollState) {
    this.scrollSubject.next(state || { scrollTop: this.host.scrollTop });
  }
  refreshMedia() {
    let type: ScreenType;
    this.width = g.win.innerWidth;
    this.height = g.win.innerHeight;

    if (this.width < 640) type = ScreenType.sm;
    else if (this.width < 976) type = ScreenType.md;
    else type = ScreenType.lg;

    this.mediaSubject.next({
      type: type
    });
  }
}
