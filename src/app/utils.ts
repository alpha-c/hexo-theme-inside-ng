import { Observable, Subject } from 'rxjs';
import { g } from './global';

interface Touchable {
  direction: 'left' | 'right';
  start: number;
  offset: number;
  isStart: boolean;
  isEnd: boolean;
}

export function base64(input: string) {
  const MAP = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
  let output = '',
    chr1, chr2, chr3, enc1, enc2, enc3, enc4,
    i = 0;

  input = utf8Encode(input);

  while (i < input.length) {
    chr1 = input.charCodeAt(i++);
    chr2 = input.charCodeAt(i++);
    chr3 = input.charCodeAt(i++);
    enc1 = chr1 >> 2;
    enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
    enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
    enc4 = chr3 & 63;
    if (isNaN(chr2)) enc3 = enc4 = 64;
    else if (isNaN(chr3)) enc4 = 64;

    output = output +
      MAP.charAt(enc1) + MAP.charAt(enc2) +
      MAP.charAt(enc3) + MAP.charAt(enc4);
  }

  return output.replace(/=/g, '');

  function utf8Encode(string) {
    string = string.replace(/\r\n/g, '\n');
    let utftext = '';
    for (let n = 0; n < string.length; n++) {
      const c = string.charCodeAt(n);
      if (c < 128)
        utftext += String.fromCharCode(c);
      else if ((c > 127) && (c < 2048)) {
        utftext += String.fromCharCode((c >> 6) | 192);
        utftext += String.fromCharCode((c & 63) | 128);
      } else {
        utftext += String.fromCharCode((c >> 12) | 224);
        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
        utftext += String.fromCharCode((c & 63) | 128);
      }

    }
    return utftext;
  }
}

const safeColorRange: number[] = [1, 255 * 3 - 48];
const colors: { [url: string]: string } = {};
export function getRGBA(imageUrl: string): Promise<string> {
  if (colors[imageUrl]) return Promise.resolve(colors[imageUrl]);

  return new Promise((resolve, reject) => {
    const image = new Image();

    image.crossOrigin = 'anonymous';
    image.onload = () => {
      const canvas = g.doc.createElement('canvas'),
        ctx = canvas.getContext('2d');
      let rgb;

      ctx.drawImage(image, 0, 0);
      rgb = ctx.getImageData(0, 0, 1, 1).data.slice(0, 3);

      const sumRGB = rgb.reduce((t, v) => t + v);
      let t = safeColorRange[1] - sumRGB;
      if (t < 0) {
        t = Math.abs(t);
        t = Math.floor(t / 3);
        rgb = rgb.map(i => i - t);
      }
      else if (sumRGB < safeColorRange[0]) rgb[0] = safeColorRange[0];

      // cache
      colors[imageUrl] = rgbToHex.apply(null, rgb);

      resolve(colors[imageUrl]);
    };
    image.onerror = reject;

    image.src = imageUrl;
  });
}

export function rgbToHex(r: number, g: number, b: number) {
  return '#' + hex(r) + hex(g) + hex(b);

  function hex(n: number) {
    const t = n.toString(16);
    return n < 16 ? ('0' + t) : t;
  }
}

export function animate(element: Element, attr: string, change: { from: number, to: number, duration?: number }, onEnd?: () => void) {
  if (change.from === change.to) {
    if (onEnd) onEnd();
    return;
  }

  if (!change.duration) {
    element[attr] = change.to;
    return;
  }

  const easing = (t, b, c, d) => c * ((t = t / d - 1) * t * t + 1) + b,
    { from, to } = change,
    during = Math.ceil((change.duration || 300) / 17) || 1;

  let start = 0,
    instance = null;

  step();

  function step() {
    const value = Math.ceil(easing(start, from, to - from, during));

    start++;
    if (start <= during) {
      element[attr] = value;
      instance = requestAnimationFrame(step);
    }
    else {
      if (onEnd) onEnd();
      cancelAnimationFrame(instance);
    }
  }
}

/**
 * touchable
 *
 * @param target
 * @param config
 */
export function touchable(target: HTMLElement, config: { scale?: number, hold?: number } = {}): Observable<Touchable> {
  const subject = new Subject<Touchable>();
  let x = 0;
  let y = 0;
  let offsetX = 0;
  let offsetY = 0;
  let effective = false;
  let direction, isStart;

  // if (!('ontouchstart' in window) && !(navigator.maxTouchPoints > 0) && !(navigator.msMaxTouchPoints > 0)) return;

  config = Object.assign({ scale: 1, hold: 5 }, config);

  target.addEventListener('touchstart', event => {
    const touch = event.touches[0];
    x = touch.clientX;
    y = touch.clientY;
    isStart = true;
  }, { passive: true });

  target.addEventListener('touchmove', event => {
    const touch = event.touches[0];
    direction = x > touch.clientX ? 'left' : 'right';
    offsetX = Math.abs(x - touch.clientX);
    offsetY = Math.abs(y - touch.clientY);

    if (offsetY >= offsetX) return;

    if (Math.abs(offsetX) > config.hold) {
      effective = true;
      subject.next({
        start: x,
        offset: getOffset(offsetX),
        direction: direction,
        isStart,
        isEnd: false
      });
      isStart = false;
    }
  }, { passive: true });

  target.addEventListener('touchend', () => {
    if (effective) subject.next({
      start: x,
      offset: getOffset(offsetX),
      direction: direction,
      isEnd: true,
      isStart: false
    });

    effective = false;
    x = y = offsetX = offsetY = 0;
  }, { passive: true });

  return subject.asObservable();

  function getOffset(value: number) {
    const out = value + (value > 0 ? -config.hold : config.hold);
    return Math.ceil(out * config.scale);
  }
}

const transitionend: string = (function () {
  const map = {
    'transition': 'transitionend',
    'webkitTransition': 'webkitTransitionEnd',
    'MozTransition': 'transitionend',
    // Opera 12
    'OTransition': 'otransitionEnd'
  } as any;

  for (const name in map) {
    if (name in g.doc.body.style) {
      return map[name];
    }
  }
})();
export function transitionEnd(element: HTMLElement, listener: (event: TransitionEvent) => void) {
  const handler = function (event) {
    if (element === event.target) {
      listener(event);
      element.removeEventListener(transitionend, handler);
    }
  };
  element.addEventListener(transitionend, handler, { passive: true });
}
const animationend: string = (function () {
  const map = {
    'animation': 'animationend',
    'webkitAnimation': 'webkitAnimationEnd',
    'MozAnimation': 'animationend',
    // Opera 12
    'OAnimation': 'oanimationend'
  } as any;

  for (const name in map) {
    if (name in g.doc.body.style) {
      return map[name];
    }
  }
})();
export function animationEnd(element: HTMLElement, listener: (event: AnimationEvent) => void) {
  const handler = function (event) {
    if (element === event.target) {
      listener(event);
      element.removeEventListener(animationend, handler);
    }
  };
  element.addEventListener(animationend, handler, { passive: true });
}

export function debounce(func, wait = 100) {
  return (...args) => {
    // const args = arguments;

    clearTimeout(func.debounce);
    func.debounce = setTimeout(() => {
      func(...args);
    }, wait);
  };
}

export function upperFirst(str: string): string {
  return str.charAt(0).toUpperCase() + str.substring(1);
}

export function kebabCase(str: string): string {
  return str.replace(/[A-Z]/g, a => '-' + a.toLowerCase());
}

const properties: {
  [key: string]: [string, string];
} = {};
export function xProperty(key: string, isCss?: boolean): string {
  if (properties[key]) return properties[key][isCss ? 1 : 0];

  const map = ['', 'webkit', 'Moz', 'O', 'ms'];
  const style = g.doc.body.style;
  const upperKey = upperFirst(key);

  for (let i = 0; i < map.length; i++) {
    const e = !map[i] ? key : (map[i] + upperKey);
    if (e in style) {
      properties[key] = [e, kebabCase(e)];
      return properties[key][isCss ? 1 : 0];
    }
  }
}

export function css(element: HTMLElement, styles: CSSStyleDeclaration): void {
  for (const key in styles) {
    element.style[key] = styles[key];
  }
}

export function sprintf(template: string, payload: (string | number)[] | { [key: string]: string | number } | number | string): string {
  if (!payload) return template;
  else if (Array.isArray(payload)) {
    let i = 0;
    return template.replace(/%(s|d)/g, () => <string>payload[i++]);
  } else if (typeof payload === 'string' || typeof payload === 'number') {
    return sprintf(template, [payload]);
  } else {
    for (const key in payload) {
      template = template.replace(new RegExp(':' + key, 'g'), <string>payload[key]);
    }
    return template;
  }
}

export function getData(raw: any, path = '') {
  if (!path || !raw) return;

  const split = path.split(/(?=\[\w+\])|\./g);

  let key = split.shift();
  let current = Object.assign(raw);
  while (key) {
    if (/\[\w+\]/.test(key)) {
      current = current[key.substring(1, key.length - 1)];
    }
    else {
      current = current[key];
    }

    if (current) {
      key = split.shift();
    } else break;
  }

  return current;
}

export function redraw(element: HTMLElement): void {
  if (element.offsetHeight < 0) alert();
}

export function random(max: number, min = 0): number {
  return min + Math.floor(Math.random() * (max - min + 1));
}
