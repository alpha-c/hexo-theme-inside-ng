import styles from './index.scss'

const clsPrefix = 'φ';
const orders = ['common']
const regex = new RegExp(`^φ(${orders.join('|')})`)
const getId = alphabetId()
const css = Object.keys(styles)
  .sort((a, b) => {
    const aMatch = a.match(regex),
      bMatch = b.match(regex)

    if (aMatch && !bMatch) return -1
    if (!aMatch && bMatch) return 1

    if (aMatch && bMatch) return orders.indexOf(aMatch[1]) - orders.indexOf(bMatch[1])

    return a > b ? 1 : -1
  })
  .reduce((o, k) => {
    o[k] = clsPrefix + getId(k)
    return o
  }, {})

/**
 * [theme, common, rest]
 */
export default css

function alphabet(i) {
  if (i < 26)
    return String.fromCharCode(97 + i)

  return alphabet(~~(i / 26)) + alphabet(i % 26)
}

/**
 * @returns {(token:string) => string}
 */
function alphabetId(start) {
  const cache = {}
  let i = start || 0
  return function (token) {
    if (cache[token]) return cache[token]
    cache[token] = alphabet(i++)
    return cache[token]
  }
}
