import { Appearance, Config } from '../interface';
import { InsideEvent } from '../interface/common';

interface LocalConfig {
  theme: Appearance;
  hash: string;
}

const color_hex_regex = /#(?:[0-9a-fA-F]{3}){1,2}/;
const color_rgb_regex = /rgb\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*\)/;
const INSIDE = '__inside__';

const fontBase = '-apple-system, BlinkMacSystemFont, "Segoe UI", Helvetica, Arial, sans-serif';
const defaults: Appearance = {
  accent_color: '#2a2b33',
  foreground_color: '#363636',
  border_color: '#e0e0e0',
  background: '#f3f6f7',
  sidebar_background: '#2a2b33',
  card_background: '#fff',
  content_width: '660px',
  'font.base': fontBase,
  'font.logo': fontBase,
  'font.menu': fontBase,
  'font.heading': fontBase,
  'font.label': fontBase,
  'font.code': fontBase,
  'font.print': fontBase,
  highlight: [
    '#ffffff', '#e0e0e0', '#f3f6f7', '#95a5b3',
    '#363636', '#262b2f', '#000000', '#000000',
    '#5d6c7b', '#40464a', '#2980b9', '#be516e',
    '#237dac', '#944770', '#239371', '#edf0f3'
  ]
};

const hasDoc = typeof document !== 'undefined';

if (hasDoc) {
  document.addEventListener('inside:theme', function (ev: CustomEvent<Appearance>) {
    document.dispatchEvent(new CustomEvent('inside', {
      detail: { type: 'theme', data: ev.detail }
    }));
  });

  document.addEventListener('inside', function handler(ev: CustomEvent<InsideEvent>) {
    if (!hasDoc || ev.detail.type !== 'theme') return false;
    changeTheme(ev.detail.data as any);
  });

  const localConfig = getLocalConfig();
  const init = () => {
    localStorage.removeItem(INSIDE);
    changeTheme({
      ...window[INSIDE].theme.default,
      name: 'default'
    }, true);
  }
  if (localConfig) {
    if (localConfig.hash === (window[INSIDE] || {}).hash) {
      changeTheme(localConfig.theme);
    } else {
      init();
    }
  } else {
    init();
  }
}

function getLocalConfig(): LocalConfig {
  const localConfig = localStorage.getItem(INSIDE);
  if (!localConfig) return;
  try {
    return JSON.parse(localConfig) as LocalConfig;
  } catch { }
}

function changeTheme(appearance: Appearance, isInit?: boolean) {
  let themeTag: HTMLStyleElement = document.querySelector('style[is="theme"]');
  const metaTag: HTMLMetaElement = document.querySelector('meta[name="theme-color"]');
  const config: Config = window[INSIDE] || {};

  if (!themeTag) {
    themeTag = document.createElement('style');
    themeTag.setAttribute('is', 'theme');
    document.body.appendChild(themeTag);
  }

  let newName = appearance.name || getLocalConfig()?.theme?.name;

  const themeKey = newName === 'dark' ? 'dark' : 'default';

  // update window.__inside__ for incremental theming
  config.theme[themeKey] = Object.assign({}, config.theme.default, config.theme[themeKey], appearance);
  // extract color from background setting
  // [sidebar bg, body bg] | [sidebar bg] | [body bg]
  // [color with sidebar open, color with sidebar close]
  config.color = [parseBackground(config.theme[themeKey].sidebar_background).color || config.theme[themeKey].accent_color]
    .concat(parseBackground(config.theme[themeKey].background).color || []);

  if (!isInit || !themeTag.innerHTML) {
    themeTag.innerHTML = css(config.theme[themeKey]) as string;

    if (metaTag) {
      metaTag.content = config.color[config.color.length - 1];
    }
  }

  localStorage.setItem(INSIDE, JSON.stringify({
    theme: config.theme[themeKey],
    hash: config.hash,
  }));
}

export function css(appearance: Appearance) {
  if (!appearance) return '';
  const config = mergeConfig(appearance, defaults);

  // An internal color used on category navigator boxShadow
  const card_color = (config.card_background.match(color_hex_regex)
    || config.card_background.match(color_rgb_regex)
    || [])[0] || config.foreground_color;

  const color = parseColor(config.accent_color);

  const entries = configToVars({
    ...config,
    card_color,
    accent_color_005: `rgba(${color.r},${color.g},${color.b},.05)`,
    accent_color_01: `rgba(${color.r},${color.g},${color.b},.1)`,
    accent_color_02: `rgba(${color.r},${color.g},${color.b},.2)`,
    accent_color_04: `rgba(${color.r},${color.g},${color.b},.4)`,
    accent_color_08: `rgba(${color.r},${color.g},${color.b},.8)`,
    accent_color: color.hex,
  }, 'inside');

  if (process.env.NODE_ENV !== 'production' && arguments[1]) {
    return entries.map(i => [i[2].toUpperCase(), `var(${i[0]})`]);
  }

  return `html{${entries.map(i => i.slice(0, 2).join(':')).join(';')}}`;
}

function parseBg(bg: string): string {
  const img_regex = /(^data:image)|(^[^\(^'^"]*\.(jpg|png|gif|svg))/;
  const bgs = bg.split(/\s+/);

  return bgs.map(s => s.match(img_regex) ? `url(${s})` : s).join(' ');
}

/**
 * @param color rgb or hex
 * @param defaultColor fallback color
 * @returns
 */
function parseColor(color: string, defaultColor?: string) {
  color = (color || '').trim();
  if (color_hex_regex.test(color))
    return { hex: color, ...hexToRgb(color) };

  if (color_rgb_regex.test(color)) {
    const rgb = color.match(color_rgb_regex).slice(1, 4).map(i => +i).filter(i => i < 256);
    if (rgb.length === 3)
      return { hex: rgbToHex.apply(null, rgb), r: rgb[0], g: rgb[1], b: rgb[2] };
  }

  if (defaultColor)
    return { hex: defaultColor, ...hexToRgb(defaultColor) };

  return {};
}

// https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
function rgbToHex(r, g, b) {
  return '#' + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}
function hexToRgb(hex) {
  // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
  const shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
  hex = hex.replace(shorthandRegex, function (m, r, g, b) {
    return r + r + g + g + b + b;
  });

  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16)
  } : null;
}

/**
 * Return font set without duplicates
 *
 * @param input     Eg. 'Monda, Roboto'
 * @param baseSet   ['arial', 'sans-serif']
 * @param returnSet Return array instead of string
 */
function mergeFontSet(input: string, baseSet: string[] = []): string[] {
  const inputSet = input ? input.split(',').map(i => i.trim()).filter(i => i) : [];

  return Array.from(new Set(inputSet.concat(baseSet)));
}

function replaceBase16(css: string, colors: string[]): string {
  return colors.reduce((pv, v, index) => {
    const basen_regex = new RegExp('base0' + index.toString(16).toUpperCase(), 'g');
    return pv.replace(basen_regex, v);
  }, css);
}

function mergeConfig(config: Appearance, defaultConfig: Appearance): Appearance {
  const ret = <Appearance>{};
  const {
    accent_color,
    foreground_color,
    border_color,
    background,
    sidebar_background,
    card_background,
    content_width,
    highlight,
    ...font
  } = config;

  // color
  ret.accent_color = accent_color ? parseColor(accent_color).hex : defaultConfig.accent_color;
  ret.foreground_color = foreground_color ? parseColor(foreground_color).hex : defaultConfig.foreground_color;
  ret.border_color = border_color ? parseColor(border_color).hex : defaultConfig.border_color;

  // background
  ret.background = background ? parseBg(background) : defaultConfig.background;
  ret.sidebar_background = sidebar_background
    ? parseBg(sidebar_background)
    : (ret.accent_color || defaultConfig.sidebar_background);
  ret.card_background = card_background ? parseBg(card_background) : defaultConfig.card_background;

  // font
  const baseSet = font['font.base'] ? mergeFontSet(font['font.base']) : defaultConfig['font.base'].split(',');
  ret['font.base'] = String(baseSet);
  ret['font.logo'] = font['font.logo'] ? mergeFontSet(font['font.logo'], baseSet).join(',') : defaultConfig['font.logo'];
  ret['font.menu'] = font['font.menu'] ? mergeFontSet(font['font.menu'], baseSet).join(',') : defaultConfig['font.menu'];
  ret['font.heading'] = font['font.heading'] ? mergeFontSet(font['font.heading'], baseSet).join(',') : defaultConfig['font.heading'];
  ret['font.label'] = font['font.label'] ? mergeFontSet(font['font.label'], baseSet).join(',') : defaultConfig['font.label'];
  ret['font.code'] = font['font.code'] ? mergeFontSet(font['font.code']).join(',') : defaultConfig['font.code'];
  ret['font.print'] = font['font.print'] ? mergeFontSet(font['font.print']).join(',') : defaultConfig['font.print'];

  // content_width
  const contentWidth = parseUnit(content_width);
  ret.content_width = contentWidth ? contentWidth.value + contentWidth.unit : defaultConfig.content_width;

  // highlight
  if (Array.isArray(highlight)) {
    ret.highlight = highlight.map(c => parseColor(c).hex).filter(c => c);
    if (ret.highlight.length < 16) ret.highlight = defaultConfig.highlight;
  } else ret.highlight = defaultConfig.highlight;

  return ret;
}

function parseUnit(value: string | number): void | { value: number; unit: '%' | 'rem' | 'px' } {
  if (!value) return;
  if (typeof value === 'number') {
    return { value, unit: 'px' };
  }

  const match = value.match(/^(\d+)(%|rem|px)$/);
  if (match) {
    return {
      value: +match[1],
      unit: match[2] as any
    };
  }
}

/**
 * Parse css background, only support hex color
 * https://github.com/ikeq/hexo-theme-inside/blob/master/lib/utils.js#L663
 *
 * #fff url => { color: '#fff', image: url }
 *
 * @param {string} value
 * @return {{color?: string; image?: string}}
 */
function parseBackground(value) {
  if (!value) return {};
  const part = value.split(/\s+/);
  const ret = {};

  // color at start
  if (color_hex_regex.test(part[0]))
    return {
      color: part[0],
      image: part.slice(1).join(' ')
    };

  // color at end
  const lastIndex = part.length - 1;
  if (part[lastIndex] && color_hex_regex.test(part[lastIndex]))
    return {
      color: part.pop(),
      image: part.join(' ')
    };

  return {
    image: value
  };
}

function configToVars(config = {}, ns = 'var') {
  return run(config);

  function run(obj, ret: Array<string[]> = [], prefix?: string) {
    for (const k in obj) {
      const v = obj[k];
      if (Array.isArray(v)) {
        v.forEach((i, $i) => {
          let t = $i.toString(16);
          if (t.length < 2) t = `0${t}`;
          return run({ [t]: i }, ret, k);
        });
      } else if (typeof v === 'object') {
        run(v, ret, k);
      } else {
        const key = (prefix ? prefix + '_' : '') + k;
        ret.push([`--${ns}-${key.replace(/[\._]/g, '-')}`, v, key]);
      }
    }
    return ret;
  }
}
