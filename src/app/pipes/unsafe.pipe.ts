import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'unsafe'
})
export class UnsafePipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) { }

  transform(value: string, type = 'html') {
    if (type === 'url') {
      return this.sanitizer.bypassSecurityTrustUrl(value);
    }

    return this.sanitizer.bypassSecurityTrustHtml(value);
  }

}
